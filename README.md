# serenity-java-cucumber

# Stack:
1. Framework: Java Serenity
2. Cucumber/Gherkin
3. Serenity Reports

# How to run tests:
	- To run tests use the following command: mvn clean install

# How to add tests:
    1. Inside of "Features" folder in Tests project create new feature file and describe scenario there, or add required steps in existing feature file
    2. Right-click inside the feature file and left-click "Generate Step Definitions"
    3. Save new Java class in required place inside of "Steps" package in src/test/java/resources folder
    4. Run the command: mvn clean install. You can find more detailed instructions here "https://cucumber.io/"

# Serenity Reports: 
 - Navigate to target/site/serenity/index.html to see the report

# CI/CD:
- Two Stages: 
            - test (Test Run)
            - deploy (Report is generated in Job Artifacts)



