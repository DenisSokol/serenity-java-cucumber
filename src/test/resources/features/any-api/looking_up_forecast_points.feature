Feature: Looking up forecast points

  Scenario Outline: Looking up forecast points by language and Yacht Club ID
    When I look up forecast points by <Language> for yacht club <Yacht Club ID>
    Then the resulting yacht club name should be <Yacht Club Name> and user name <User Name>
    Examples:
      | Language | Yacht Club ID | User Name | Yacht Club Name |
      | en-GB    | 23            | clara     | Alcudia Bay     |
      | fr-FR    | 8             | clara     | Alcudia Bay   |