Feature: Looking Up Topics

  Scenario Outline: Looking up topics by media type extension
    When I look up topics by <Media Type Extension>
    Then Response Status code should be <Status Code>
    Then Validate LayOut is <LayOut Name>
    Examples:
      | Media Type Extension | Status Code | LayOut Name |
      | .json                | 200         | topic       |

  Scenario Outline: Looking up topics by media type extension
    When I look up topics by <Media Type Extension>
    Then Response Status code should be <Status Code>
    Examples:
      | Media Type Extension | Status Code |
      | .xml                 | 404         |