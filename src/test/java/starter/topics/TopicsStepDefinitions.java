package starter.topics;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class TopicsStepDefinitions {
    @Steps
    TopicsAPI topicsAPI;

    @When("I look up topics by {word}")
    public void iLookUpTopicsByMediaTypeExtension(String mediaTypeExtension) {
        topicsAPI.fetchTopicsByMediaTypeExtension(mediaTypeExtension);
    }

    @Then("Response Status code should be {word}")
    public void responseStatusCodeShouldBeStatusCode(String statusCode) {
        restAssuredThat(response -> response.statusCode(Integer.parseInt(statusCode)));
    }

    @Then("Validate LayOut is {word}")
    public void validateLayOutIsLayOutName(String layOut) {
        restAssuredThat(response -> response.body(TopicsResponse.FIRST_LAYOUT, equalTo(layOut)));
    }
}