package starter.topics;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class TopicsAPI {
    private static String TOPICS_BY_MEDIA_TYPE_EXTENSION = "https://www.healthcare.gov/api/topics{mediaTypeExtension}";
    @Step("Get Topics by media type extension {0}")
    public void fetchTopicsByMediaTypeExtension(String mediaTypeExtension) {
        SerenityRest.given()
                .pathParam("mediaTypeExtension", mediaTypeExtension)
                .get(TOPICS_BY_MEDIA_TYPE_EXTENSION);
    }
}
