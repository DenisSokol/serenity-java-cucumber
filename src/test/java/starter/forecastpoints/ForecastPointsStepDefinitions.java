package starter.forecastpoints;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class ForecastPointsStepDefinitions {

    @Steps
    ForecastPointsAPI forecastPointsAPI;

    @When("I look up forecast points by {word} for yacht club {word}")
    public void lookUpForecastPoints(String language, String yachtClubId) {
        forecastPointsAPI.fetchForecastPointsByLanguageAndYachtClubId(language, yachtClubId);
    }

    @Then("the resulting yacht club name should be {} and user name {}")
    public void theResultingLocationShouldBe(String yachtName, String userName) {
        restAssuredThat(response -> response.statusCode(200));
        restAssuredThat(response -> response.body(ForecastPointsResponse.FIRST_USER_NAME, equalTo(userName)));
        restAssuredThat(response -> response.body(ForecastPointsResponse.FIRST_YACHT_NAME, equalTo(yachtName)));
    }
}
