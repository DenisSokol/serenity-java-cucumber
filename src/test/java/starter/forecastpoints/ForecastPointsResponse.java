package starter.forecastpoints;

public class ForecastPointsResponse {
    public static final String FIRST_USER_NAME = "'data'[0].'user'";
    public static final String FIRST_YACHT_NAME = "'data'[0].'name'";
}
