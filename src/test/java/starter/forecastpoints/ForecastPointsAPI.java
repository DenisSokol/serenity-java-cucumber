package starter.forecastpoints;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ForecastPointsAPI {

    private static String FORECAST_POINTS_BY_LANGUAGE_AND_YACHT_CLUB_ID = "http://api.oceandrivers.com/v1.0/getForecastPoints/{yachtClubId}/language/{language}";

    @Step("Get ForecastPoints by language {0} for yacht club {1}")
    public void fetchForecastPointsByLanguageAndYachtClubId(String language, String yachtClubId) {
        SerenityRest.given()
                .pathParam("language", language)
                .pathParam("yachtClubId", yachtClubId)
                .get(FORECAST_POINTS_BY_LANGUAGE_AND_YACHT_CLUB_ID);
    }
}
